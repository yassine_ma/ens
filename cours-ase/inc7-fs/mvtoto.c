link ("toto", "titi") ;	// crée un lien physique \frquote{titi} vers l'inode de \frquote{toto}
unlink ("toto") ;	// supprime le lien \frquote{toto} dorénavant inutile
