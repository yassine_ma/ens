// version naïve (et fausse)
void demarrer_lecture (struct desc_es *d) {
    // insertion en queue de liste (circulaire, liste pointe sur la queue)
    d->suivant = (liste == NULL) ? d : liste->suivant ;
    liste = d ;

    // s'il n'y a pas de requête en attente, démarrer la nôtre
    if (d->suivant == d) {
	demarrer_requete (d) ;
    }

    // mettre le processus (ou thread) courant en attente
    d->proc = curproc ;
    mettre_processus_en_attente () ;
}

void traiter_int_disque (void) {
    struct desc_es *d ;

    // extraire la requête la plus ancienne, i.e. celle en tête de liste
    d = liste->suivant ;		// tête de liste car circulaire
    liste = (d == liste) ? NULL : liste->suivant ;

    // mettre le processus correspondant dans l'état \frquote{prêt à tourner}
    reveiller_processus (d->proc) ;

    // s'il y a encore des requêtes en attente, démarrer la plus ancienne
    if (liste != NULL) {
	demarrer_requete (liste->suivant) ;
    }
}

// version corrigée
void demarrer_lecture (struct desc_es *d) {
    masquer_interruptions () ;

    ...		// version précédente de la fonction

    demasquer_interruptions () ;
}
